RUNING GAME
```
    Inicia o servidor 
    Gerar as classes a partir do wsimport
    E depois inicia o CLIENT
```

Inicia o servidor
```
* Imagem sobre a inicializaçao do servidor no final do WIKI. https://gitlab.com/ad-si-2015-2/projeto3-grupo5/wikis/home   
```

Gerar as classes a partir do wsimport
   Para criar as classes a partir do servidor wsdl é preciso estar na pasta do projeto3-grupo5 e executar o comando:

```
wsimport -keep -p clientws -s src http://localhost:8080/WebServices/jogo?wsdl
```


Comando usado:
``` 
wsimport [options] <wsdl>
``` 

Onde:
```
wsimport: A ferramenta wsimport gera JAX -WS artefatos portáteis, ou seja, acessa o servidor WSDL e gera as classes em java.
[options]: 
    -keep: Manter os arquivos gerados
    -p: Especificando um pacote de destino através desta opção de linha de comando , 
        substitui qualquer personalização WSDL e esquema de ligação para o nome do pacote 
        e o algoritmo nome do pacote padrão definido na especificação.
    -s: Especifica onde colocar os arquivos de origem gerados.
```

Client
```
A classe ClienteJogo.java é a classe que roda na parte Client da aplicação. Ela faz requisições ao servidor,
como por exemplo para um usuário entrar no jogo.
Há dois métodos, o imprimeMensagem que por sua vez cuida das mensagens apresentadas ao longo do jogo, e o 
outro método e o limpaTela, onde cuida de retirar o que nao precisa mais ser vizualizado da tela.
```
Server
```
Na parte servidor temos as classes Cliente.java, JogoPalito.java, Sala.java e ServidorJogo.java.

Em Cliente.java(server), há a manipulação de nome de usuário, pontuação e os metodos getId e setId que cuidam de identificar
cada cliente no jogo.

Em JogoPalito.java(server), há o acréscimo de novas salas, o registro de novos usuários, a contagem de usuários, 
acrescimo de um usuário em uma sala chamando um método na classe Sala.java(server) e a listagem das salas existentes.

Em Sala.java(server), há o acréscimo de um cliente em uma sala e há o método 'getQuantidadeDeJogadoresNaSala', 
onde você consegue saber a quantidade de jogadores em determinada sala.

Em ServidorJogo.java(server) há a inicialização do servidor nar url 'http://localhost:8080/WebServices/jogo', da classe 'JogoPalito'.
```
