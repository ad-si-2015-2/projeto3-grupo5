
package clientws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the clientws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InterruptedException_QNAME = new QName("http://servidor/", "InterruptedException");
    private final static QName _FimDoJogo_QNAME = new QName("http://servidor/", "fimDoJogo");
    private final static QName _GetMensagensDoClienteResponse_QNAME = new QName("http://servidor/", "getMensagensDoClienteResponse");
    private final static QName _RegistrarResponse_QNAME = new QName("http://servidor/", "registrarResponse");
    private final static QName _AddMensagemParaOCliente_QNAME = new QName("http://servidor/", "addMensagemParaOCliente");
    private final static QName _GetSalas_QNAME = new QName("http://servidor/", "getSalas");
    private final static QName _Registrar_QNAME = new QName("http://servidor/", "registrar");
    private final static QName _GetJogadores_QNAME = new QName("http://servidor/", "getJogadores");
    private final static QName _GravaJogadaResponse_QNAME = new QName("http://servidor/", "gravaJogadaResponse");
    private final static QName _AddNaSala_QNAME = new QName("http://servidor/", "addNaSala");
    private final static QName _GravaJogada_QNAME = new QName("http://servidor/", "gravaJogada");
    private final static QName _IniciarJogoResponse_QNAME = new QName("http://servidor/", "iniciarJogoResponse");
    private final static QName _AddMensagemParaOClienteResponse_QNAME = new QName("http://servidor/", "addMensagemParaOClienteResponse");
    private final static QName _AddNaSalaResponse_QNAME = new QName("http://servidor/", "addNaSalaResponse");
    private final static QName _IniciarJogo_QNAME = new QName("http://servidor/", "iniciarJogo");
    private final static QName _FimDoJogoResponse_QNAME = new QName("http://servidor/", "fimDoJogoResponse");
    private final static QName _AddMensagemParaClientes_QNAME = new QName("http://servidor/", "addMensagemParaClientes");
    private final static QName _GetMensagensDoCliente_QNAME = new QName("http://servidor/", "getMensagensDoCliente");
    private final static QName _GetSalasResponse_QNAME = new QName("http://servidor/", "getSalasResponse");
    private final static QName _GetJogadoresResponse_QNAME = new QName("http://servidor/", "getJogadoresResponse");
    private final static QName _AddMensagemParaClientesResponse_QNAME = new QName("http://servidor/", "addMensagemParaClientesResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: clientws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetJogadores }
     * 
     */
    public GetJogadores createGetJogadores() {
        return new GetJogadores();
    }

    /**
     * Create an instance of {@link GravaJogadaResponse }
     * 
     */
    public GravaJogadaResponse createGravaJogadaResponse() {
        return new GravaJogadaResponse();
    }

    /**
     * Create an instance of {@link Registrar }
     * 
     */
    public Registrar createRegistrar() {
        return new Registrar();
    }

    /**
     * Create an instance of {@link GetSalas }
     * 
     */
    public GetSalas createGetSalas() {
        return new GetSalas();
    }

    /**
     * Create an instance of {@link AddMensagemParaOCliente }
     * 
     */
    public AddMensagemParaOCliente createAddMensagemParaOCliente() {
        return new AddMensagemParaOCliente();
    }

    /**
     * Create an instance of {@link GetMensagensDoClienteResponse }
     * 
     */
    public GetMensagensDoClienteResponse createGetMensagensDoClienteResponse() {
        return new GetMensagensDoClienteResponse();
    }

    /**
     * Create an instance of {@link RegistrarResponse }
     * 
     */
    public RegistrarResponse createRegistrarResponse() {
        return new RegistrarResponse();
    }

    /**
     * Create an instance of {@link InterruptedException }
     * 
     */
    public InterruptedException createInterruptedException() {
        return new InterruptedException();
    }

    /**
     * Create an instance of {@link FimDoJogo }
     * 
     */
    public FimDoJogo createFimDoJogo() {
        return new FimDoJogo();
    }

    /**
     * Create an instance of {@link AddMensagemParaClientesResponse }
     * 
     */
    public AddMensagemParaClientesResponse createAddMensagemParaClientesResponse() {
        return new AddMensagemParaClientesResponse();
    }

    /**
     * Create an instance of {@link GetJogadoresResponse }
     * 
     */
    public GetJogadoresResponse createGetJogadoresResponse() {
        return new GetJogadoresResponse();
    }

    /**
     * Create an instance of {@link GetSalasResponse }
     * 
     */
    public GetSalasResponse createGetSalasResponse() {
        return new GetSalasResponse();
    }

    /**
     * Create an instance of {@link GetMensagensDoCliente }
     * 
     */
    public GetMensagensDoCliente createGetMensagensDoCliente() {
        return new GetMensagensDoCliente();
    }

    /**
     * Create an instance of {@link AddMensagemParaClientes }
     * 
     */
    public AddMensagemParaClientes createAddMensagemParaClientes() {
        return new AddMensagemParaClientes();
    }

    /**
     * Create an instance of {@link FimDoJogoResponse }
     * 
     */
    public FimDoJogoResponse createFimDoJogoResponse() {
        return new FimDoJogoResponse();
    }

    /**
     * Create an instance of {@link IniciarJogo }
     * 
     */
    public IniciarJogo createIniciarJogo() {
        return new IniciarJogo();
    }

    /**
     * Create an instance of {@link AddNaSalaResponse }
     * 
     */
    public AddNaSalaResponse createAddNaSalaResponse() {
        return new AddNaSalaResponse();
    }

    /**
     * Create an instance of {@link AddMensagemParaOClienteResponse }
     * 
     */
    public AddMensagemParaOClienteResponse createAddMensagemParaOClienteResponse() {
        return new AddMensagemParaOClienteResponse();
    }

    /**
     * Create an instance of {@link AddNaSala }
     * 
     */
    public AddNaSala createAddNaSala() {
        return new AddNaSala();
    }

    /**
     * Create an instance of {@link GravaJogada }
     * 
     */
    public GravaJogada createGravaJogada() {
        return new GravaJogada();
    }

    /**
     * Create an instance of {@link IniciarJogoResponse }
     * 
     */
    public IniciarJogoResponse createIniciarJogoResponse() {
        return new IniciarJogoResponse();
    }

    /**
     * Create an instance of {@link Cliente }
     * 
     */
    public Cliente createCliente() {
        return new Cliente();
    }

    /**
     * Create an instance of {@link Sala }
     * 
     */
    public Sala createSala() {
        return new Sala();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InterruptedException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "InterruptedException")
    public JAXBElement<InterruptedException> createInterruptedException(InterruptedException value) {
        return new JAXBElement<InterruptedException>(_InterruptedException_QNAME, InterruptedException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FimDoJogo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "fimDoJogo")
    public JAXBElement<FimDoJogo> createFimDoJogo(FimDoJogo value) {
        return new JAXBElement<FimDoJogo>(_FimDoJogo_QNAME, FimDoJogo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMensagensDoClienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getMensagensDoClienteResponse")
    public JAXBElement<GetMensagensDoClienteResponse> createGetMensagensDoClienteResponse(GetMensagensDoClienteResponse value) {
        return new JAXBElement<GetMensagensDoClienteResponse>(_GetMensagensDoClienteResponse_QNAME, GetMensagensDoClienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "registrarResponse")
    public JAXBElement<RegistrarResponse> createRegistrarResponse(RegistrarResponse value) {
        return new JAXBElement<RegistrarResponse>(_RegistrarResponse_QNAME, RegistrarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddMensagemParaOCliente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "addMensagemParaOCliente")
    public JAXBElement<AddMensagemParaOCliente> createAddMensagemParaOCliente(AddMensagemParaOCliente value) {
        return new JAXBElement<AddMensagemParaOCliente>(_AddMensagemParaOCliente_QNAME, AddMensagemParaOCliente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSalas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getSalas")
    public JAXBElement<GetSalas> createGetSalas(GetSalas value) {
        return new JAXBElement<GetSalas>(_GetSalas_QNAME, GetSalas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Registrar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "registrar")
    public JAXBElement<Registrar> createRegistrar(Registrar value) {
        return new JAXBElement<Registrar>(_Registrar_QNAME, Registrar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJogadores }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getJogadores")
    public JAXBElement<GetJogadores> createGetJogadores(GetJogadores value) {
        return new JAXBElement<GetJogadores>(_GetJogadores_QNAME, GetJogadores.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GravaJogadaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "gravaJogadaResponse")
    public JAXBElement<GravaJogadaResponse> createGravaJogadaResponse(GravaJogadaResponse value) {
        return new JAXBElement<GravaJogadaResponse>(_GravaJogadaResponse_QNAME, GravaJogadaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddNaSala }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "addNaSala")
    public JAXBElement<AddNaSala> createAddNaSala(AddNaSala value) {
        return new JAXBElement<AddNaSala>(_AddNaSala_QNAME, AddNaSala.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GravaJogada }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "gravaJogada")
    public JAXBElement<GravaJogada> createGravaJogada(GravaJogada value) {
        return new JAXBElement<GravaJogada>(_GravaJogada_QNAME, GravaJogada.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IniciarJogoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "iniciarJogoResponse")
    public JAXBElement<IniciarJogoResponse> createIniciarJogoResponse(IniciarJogoResponse value) {
        return new JAXBElement<IniciarJogoResponse>(_IniciarJogoResponse_QNAME, IniciarJogoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddMensagemParaOClienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "addMensagemParaOClienteResponse")
    public JAXBElement<AddMensagemParaOClienteResponse> createAddMensagemParaOClienteResponse(AddMensagemParaOClienteResponse value) {
        return new JAXBElement<AddMensagemParaOClienteResponse>(_AddMensagemParaOClienteResponse_QNAME, AddMensagemParaOClienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddNaSalaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "addNaSalaResponse")
    public JAXBElement<AddNaSalaResponse> createAddNaSalaResponse(AddNaSalaResponse value) {
        return new JAXBElement<AddNaSalaResponse>(_AddNaSalaResponse_QNAME, AddNaSalaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IniciarJogo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "iniciarJogo")
    public JAXBElement<IniciarJogo> createIniciarJogo(IniciarJogo value) {
        return new JAXBElement<IniciarJogo>(_IniciarJogo_QNAME, IniciarJogo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FimDoJogoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "fimDoJogoResponse")
    public JAXBElement<FimDoJogoResponse> createFimDoJogoResponse(FimDoJogoResponse value) {
        return new JAXBElement<FimDoJogoResponse>(_FimDoJogoResponse_QNAME, FimDoJogoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddMensagemParaClientes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "addMensagemParaClientes")
    public JAXBElement<AddMensagemParaClientes> createAddMensagemParaClientes(AddMensagemParaClientes value) {
        return new JAXBElement<AddMensagemParaClientes>(_AddMensagemParaClientes_QNAME, AddMensagemParaClientes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMensagensDoCliente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getMensagensDoCliente")
    public JAXBElement<GetMensagensDoCliente> createGetMensagensDoCliente(GetMensagensDoCliente value) {
        return new JAXBElement<GetMensagensDoCliente>(_GetMensagensDoCliente_QNAME, GetMensagensDoCliente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSalasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getSalasResponse")
    public JAXBElement<GetSalasResponse> createGetSalasResponse(GetSalasResponse value) {
        return new JAXBElement<GetSalasResponse>(_GetSalasResponse_QNAME, GetSalasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJogadoresResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getJogadoresResponse")
    public JAXBElement<GetJogadoresResponse> createGetJogadoresResponse(GetJogadoresResponse value) {
        return new JAXBElement<GetJogadoresResponse>(_GetJogadoresResponse_QNAME, GetJogadoresResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddMensagemParaClientesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "addMensagemParaClientesResponse")
    public JAXBElement<AddMensagemParaClientesResponse> createAddMensagemParaClientesResponse(AddMensagemParaClientesResponse value) {
        return new JAXBElement<AddMensagemParaClientesResponse>(_AddMensagemParaClientesResponse_QNAME, AddMensagemParaClientesResponse.class, null, value);
    }

}
