package servidor;

import java.util.ArrayList;
import java.util.List;

public class Sala {
	String nome;
	List<Cliente> jogadores = new ArrayList();
	Boolean fimDoJogo = false;
	int status = 0;
	int rodada;
	public Sala(String nome){
		this.nome = nome;
		this.rodada = 0;
	}
	

	public int getRodada() {
		return rodada;
	}


	public void setRodada(int rodada) {
		this.rodada = rodada;
	}


	public void addNaSala(Cliente client){
		jogadores.add(client);
	}
	
	public int getQuantidadeDeJogadoresNaSala(){
		return this.jogadores.size();
	}

	public Boolean fimDoJogo() {
		return this.fimDoJogo;
	}

	public void IniciaSala() {
		this.status = 1;
	}
	public int getStatusSala(){
	  return this.status;
	}
	public List<Cliente> getJogadores(){
		return this.jogadores;
	}
}
