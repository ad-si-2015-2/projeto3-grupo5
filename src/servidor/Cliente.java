/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author alunoufg
 */
public class Cliente {
	
	public Cliente() {
		this.nome = "sem nome";
		this.pontuacao = 0;
		this.mensagens = new ArrayList<String>();
	}

	public Cliente(String nome, int pontuacao) {
		this.nome = nome;
		this.pontuacao = pontuacao;
		this.mensagens.add("Seja bem vindo " + nome);
	}
	
	public String getNome() {
		return nome;
	}

	public int getPontuacao() {
		return pontuacao;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}

	public void setPalpite(int palpite) {
		this.palpite = palpite;
	}
	
	public int getPalpite() {
		return palpite;
	}

	public void addMensagens(String msg) {
		this.mensagens.add(msg);
	}
	
	public List<String> getMensagens() {
		List<String> m = mensagens;
		mensagens = new ArrayList<String>();
		return m;
	}
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	private String nome;
	private int id=-1;
	private int pontuacao;
	private Boolean status;
	

	private int palpite;
	public List<String> mensagens;;

}
