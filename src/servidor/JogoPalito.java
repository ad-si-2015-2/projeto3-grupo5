package servidor;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

@WebService
public class JogoPalito {
	List<Cliente> clientes = new ArrayList();
	List<Sala> salas = new ArrayList();
	int quantidadeDeSalas = 10;

	public JogoPalito() {
		for (int i = 0; i < this.quantidadeDeSalas; i++) {
			salas.add(new Sala("Sala " + i));
		}
	}

	public int registrar(Cliente cliente) {
		int id;
		if (clientes.size() > 0) {
			Cliente c = clientes.get(clientes.size() - 1);
			id = c.getId() + 1;
		} else {
			id = 1;
		}
		cliente.setId(id);
		clientes.add(cliente);
		this.mostraSalas(cliente);

		return id;
	}

	private void mostraSalas(Cliente cliente) {
		addMensagemParaOCliente("/limpaTela", cliente);
		addMensagemParaOCliente("Digite a sala que deseja entrar:", cliente);
		for (int i = 0; i < salas.size(); i++) {
			String status = null;
			Sala ss = salas.get(i);
			if (ss.getStatusSala() == 0) {
				addMensagemParaOCliente(
						"Sala " + i + "  -  " + ss.getQuantidadeDeJogadoresNaSala() + " jogadores na sala", cliente);
			} else if (ss.getStatusSala() == 1) {
				addMensagemParaOCliente("Sala " + i + "  -  " + ss.getQuantidadeDeJogadoresNaSala()
						+ " jogadores na sala - Jogo em andamento", cliente);
			}
		}
	}

	public int getJogadores() {
		return clientes.size();
	}

	public List<Sala> getSalas() {
		return salas;
	}

	public void addNaSala(int sala, Cliente cliente) {
		salas.get(sala).addNaSala(cliente);
		if (salas.get(sala).getQuantidadeDeJogadoresNaSala() == 1) {
			addMensagemParaOCliente(cliente.getNome() + ", voc� entrou na sala " + sala + "\n"
					+ "Voc� � o administrador," + "\n" + "Digite: '/iniciar' para iniciar o jogo!", cliente);
			addMensagemParaOCliente("/grupo5_inicieOJogo", cliente);
		} else {
			addMensagemParaOCliente(cliente.getNome() + ", voc� entrou na sala " + sala + "\n"
					+ "Voc� n�o � o administrador!" + "\n" + "O administrador vai iniciar a partida em instantes!",
					cliente);
		}
	}

	public void addMensagemParaOCliente(String mensagem, Cliente client) {
		for (int i = 0; i < this.clientes.size(); i++) {
			if (this.clientes.get(i).getId() == client.getId()) {
				this.clientes.get(i).addMensagens(mensagem);
			}
		}
	}

	public void addMensagemParaClientes(String mensagem, Sala sala) {
		for (int j = 0; j < sala.getJogadores().size(); j++) {
			addMensagemParaOCliente(mensagem, sala.getJogadores().get(j));
		}
	}

	public void iniciarJogo(int numSala) throws InterruptedException {
		
		this.salas.get(numSala).IniciaSala();
		this.salas.get(numSala).setRodada(this.salas.get(numSala).getRodada() + 1);
		addMensagemParaClientes("/limpaTela", this.salas.get(numSala));
		addMensagemParaClientes("Jogo iniciado", this.salas.get(numSala));
		while (!fimDoJogo(numSala)) {
			addMensagemParaClientes("---------Inicio da Rodada " + this.salas.get(numSala).getRodada(), this.salas.get(numSala));
			for (int i = 0; i < this.salas.get(numSala).getJogadores().size(); i++) {
				if(this.salas.get(numSala).getJogadores().get(i).getStatus() != null){
					while (this.salas.get(numSala).getJogadores().get(i).getStatus()) {
						Thread.sleep(400);
					}
				}
				addMensagemParaOCliente("Sua vez de jogar d� o seu palpite!",
						this.salas.get(numSala).getJogadores().get(i));
				addMensagemParaOCliente("/grupo5_suavez", this.salas.get(numSala).getJogadores().get(i));
				this.salas.get(numSala).getJogadores().get(i).setStatus(true);
				final Cliente jg = this.salas.get(numSala).getJogadores().get(i);
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							Thread.sleep(30000);
							if (jg.getStatus()) {
								jg.setStatus(false);
								jg.addMensagens("Seu tempo expirou!");
							}
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}).start();
			}
			
			verificaVencedorDaRodada(numSala);
		}
	}

	private void verificaVencedorDaRodada(int numSala) {
		getSomaDosPalitos(numSala);
		for (int i = 0; i < this.salas.get(numSala).getJogadores().size(); i++) {
			this.salas.get(numSala).getJogadores().get(i).getPalpite();
		}
	}

	private int getSomaDosPalitos(int numSala) {
		int somaPalites = 0;
		for (int i = 0; i < this.salas.get(numSala).getJogadores().size(); i++) {
			somaPalites += this.salas.get(numSala).getJogadores().get(i).getPalpite();
		}
		return somaPalites;
		
	}

	public void gravaJogada(Cliente cliente, int palpite) {
		cliente.setPalpite(palpite);
		cliente.setStatus(false);
	}

	public List<String> getMensagensDoCliente(Cliente cliente) {
		List<String> msgs = null;
		for (int i = 0; i < this.clientes.size(); i++) {
			if (this.clientes.get(i).getId() == cliente.getId()) {
				msgs = this.clientes.get(i).getMensagens();
			}
		}
		return msgs;
	}

	public Boolean fimDoJogo(int numSala) {
		return salas.get(numSala).fimDoJogo();
	}

}
