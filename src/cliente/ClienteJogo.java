/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import clientws.Cliente;
import clientws.InterruptedException_Exception;
import clientws.JogoPalito;
import clientws.JogoPalitoService;
import clientws.Sala;

/**
 *
 * @author alunoufg
 */
public class ClienteJogo {
	public static void main(String[] args) throws InterruptedException, InterruptedException_Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		JogoPalitoService jas = new JogoPalitoService();
		JogoPalito port = jas.getPort(JogoPalito.class);
		System.out.println("Informe seu nome: ");
		Cliente cliente = new Cliente();
		try {
			cliente.setNome(reader.readLine());
			cliente.setPontuacao(0);
			int id = port.registrar(cliente);
			cliente.setId(id);
			imprimeMensagens(port.getMensagensDoCliente(cliente), port, -1, cliente);
			int numSala = Integer.parseInt(reader.readLine());
			port.addNaSala(numSala, cliente);
			limpaTela();

			while (!port.fimDoJogo(numSala)) {
				Thread.sleep(400);
				List<String> m = port.getMensagensDoCliente(cliente);
				if (m.size() > 0) {
					imprimeMensagens(m, port, numSala, cliente);
				}
			}

		} catch (IOException ex) {
			Logger.getLogger(ClienteJogo.class.getName()).log(Level.SEVERE, null, ex);
		}
		System.out.println(port.getJogadores());
	}

	private static String lerEscritaDoCliente() throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		return reader.readLine();
	}

	private static void imprimeMensagens(List<String> mensagem, JogoPalito port, int numSala, Cliente cliente) throws IOException, InterruptedException_Exception {
		if (mensagem.size() > 0)
			for (int i = 0; i < mensagem.size(); i++) {
				String m = mensagem.get(i);
				if (m.equals("/limpaTela")) {
					limpaTela();
				}
				else if (m.equals("/grupo5_suavez")) {
					String r = lerEscritaDoCliente();
					port.gravaJogada(cliente, Integer.parseInt(r));
				} else if (m.equals("/grupo5_inicieOJogo")) {
					String r = lerEscritaDoCliente();
					while (!r.equals("/iniciar")) {
						r = lerEscritaDoCliente();
					}
					new Thread(new Runnable() {
						@Override
						public void run() {
							try {
								port.iniciarJogo(numSala); 
							} catch (InterruptedException_Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}).start();
					
				} else
					System.out.println(m);
			}
	}

	private static void limpaTela() {
		for (int i = 0; i < 100; i++) {
			System.out.println("");
		}
	}
}